# Orders App (setel fullstack)

Application for order module.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* mongodb : https://docs.mongodb.com/manual/installation/
* Payment application: https://bitbucket.org/creativesparkapp/payments-application/src/master/
* Auth application: https://bitbucket.org/creativesparkapp/auth-app/src/master/


### Installing

* Clone the repo

* configure database connection base on mongodb setup at

```
	/src/database/database.providers.ts
```
* run 
```
	$ npm install
	$ npm run start:dev
```

## Test
* open url http://localhost:3000/api to see api documentation

## Built With

* [NestJS](https://docs.nestjs.com/) - The NodeJs framework used


## Authors

* **Burhanuddin Helmy** *

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

